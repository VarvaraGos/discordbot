var Discord = require('discord.io');
var logger = require('winston');
var auth = require('./auth.json');
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});
bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});
bot.on('message', function (user, userID, channelID, message, evt) {

	var args = message.split(' ');
        var cmd = args[0];
       
        args = args.splice(1);
        switch(cmd) {
            // Hello
            case '!ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
            break;
            case 'Hello':
                bot.sendMessage({
                    to: channelID,
                    message: 'Hello, world!'
                });
            break;
	    case 'Hi':
                bot.sendMessage({
                    to: channelID,
                    message: 'Hi, everyone!'
                });
            break;
            // Just add any case commands if you want to..
         }
});